# Power profile cycle

Script to cycle through power-profiles-daemon profiles. Handy for integration
with waybar, i3blocks and others. When run it will cycle to next profile and
output a corresponding fa-icon for displaying in a bar. With the -m toggle,
the script will not cycle profiles, and rather just print an fa-icon corresponding to the
current profile.

Depends on power-profiles-daemon and font awesome icons.

To compliment this, my waybar config has a custom module that shows the output:

```
    "custom/power_profile": {
        "exec": "sleep 0.3 && /home/lassegs/dev/powerprofilecycle/powerprofilecycle.sh -m",
        "interval": 120,
        "on-click": "/home/lassegs/dev/powerprofilecycle.sh",
        "exec-on-event": true
    },
```

_Without the `sleep 0.3` the output wouldn't consistently show the updated icon..._
